#ifndef LED_CONTROL_H_
#define LED_CONTROL_H_

bool led_exported(char ledId[]);
void set_led_brightness(char ledId[], int brightness);

bool led_exported(char ledId[]) {
  char ledStatusCmd[80];
  sprintf(ledStatusCmd, "/sys/class/leds/airhome:%s/brightness", ledId);
  puts(ledStatusCmd);
  int status = access(ledStatusCmd, F_OK );
  if (status == -1) {
      printf("File for led%s doesn't exist. Please update the device tree file.\n", ledId);
      return false;
  } else {
    printf("LOG: led%s was successfully configured", ledId);
    return true;
  }
}


void set_led_brightness(char ledId[], int brightness) {
  char ledOnCmd[80];
  sprintf(ledOnCmd, "echo %d > /sys/class/leds/airhome:%s/brightness", brightness, ledId);
  system(ledOnCmd);
}

#endif
