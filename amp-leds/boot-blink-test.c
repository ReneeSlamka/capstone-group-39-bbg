#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/time.h>
#include <regex.h>
#include <stdbool.h>
#include "gpio-led-control.h"

#define LED1RED 66

void sig_handler(int signo)
{
    if (signo == SIGINT)
        printf("\nreceived SIGINT\n");
    set_gpio_value(LED1RED, 0);
    exit(1);
}

void blink_led(int gpioNum) {
  unsigned int cnt = 0;
  while(1) {
    if (cnt % 2 == 0) {
      set_gpio_value(gpioNum, 1);
    } else {
      set_gpio_value(gpioNum, 0);
    }
    cnt += 1;
    if (cnt == 8) {
      break;
    }
    sleep(1);
  }
}

int main(int argc, char const *argv[]) {

  if (signal(SIGINT, sig_handler) == SIG_ERR) {
      printf("\ncan't catch SIG Handler\n");
      exit(1);
  }

  set_up_gpio(LED1RED, "out", 0, 0);
  blink_led(LED1RED);

  return 0;
}
