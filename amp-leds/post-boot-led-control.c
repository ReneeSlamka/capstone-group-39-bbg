#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/time.h>
#include <regex.h>
#include <stdbool.h>
#include "led-control.h"
#include "state-monitor.h"

#define LED1RED "red1"
#define LED1BLUE "blue1"
#define LED1GREEN "green1"
#define LED2RED "red2"
#define LED2BLUE "blue2"
#define LED2GREEN "green2"
#define ON 0
#define OFF 1


void sig_handler(int signo)
{
    if (signo == SIGINT)
        printf("\nreceived SIGINT\n");
    set_led_brightness(LED2RED, OFF);
    set_led_brightness(LED1BLUE, OFF);
    set_led_brightness(LED2GREEN, OFF);
    exit(1);
}


void power_off_all_leds() {
  set_led_brightness(LED1BLUE, OFF);
  set_led_brightness(LED2RED, OFF);
  set_led_brightness(LED2GREEN, OFF);
}

void connecting_to_network_mode() {
  power_off_all_leds();
  int timeCounterSeconds = 0;
  while (timeCounterSeconds < 15 || (!check_network_connected())) {
    if (timeCounterSeconds % 2 == 0) {
      set_led_brightness(LED2RED, ON);
    } else {
      set_led_brightness(LED2RED, OFF);
    }
    timeCounterSeconds++;
    sleep(1);
  }
}

void hotspot_mode() {
  power_off_all_leds();
  set_led_brightness(LED1RED, ON);
  int counter = 0;
  while (true) {
    if (counter % 2 == 0) {
      set_led_brightness(LED2RED, ON);
    } else {
      set_led_brightness(LED2RED, OFF);
    }
    if (check_user_joined_hotspot()) {
      set_led_brightness(LED2RED, OFF);
      /* A user has joined the hotspot at this point */
      set_led_brightness(LED2RED, OFF);

      while (check_user_joined_hotspot()) {
        if (check_network_config_settings()) {
          break;
        }
      }
      /* Second check just to break out of parent while loop */
      if (check_network_config_settings()) {
        break;
      }
    }
    counter++;
    sleep(1);
  }

  return;
}


int main(int argc, char const *argv[]) {

  if (signal(SIGINT, sig_handler) == SIG_ERR) {
      printf("\ncan't catch SIG Handler\n");
      exit(1);
  }

  /* Check that all required LEDs have already been set up, else exit */
  if (!(led_exported(LED2RED) && led_exported(LED1BLUE) && led_exported(LED2GREEN))) {
    printf("[ERROR]: Some LEDs have not been configured correctly, program terminating.\n");
    exit(1);
  }

  /*Mode 1: Boot mode - LED2RED solid - on by default at boot */
  while (!check_boot_complete()) {
    sleep(1); //So we're not busy waiting the entire time?
  }

  power_off_all_leds();

  while (true) {
    /*Mode 2: Connecting to network(LED2RED blinking) - allow ~15 seconds to attempt this*/
    if (!check_network_connected()) {
      connecting_to_network_mode();
    }

    if (check_network_connected()) {
      /*Mode 3A: Connected to network (LED1BLUE solid)*/
      set_led_brightness(LED2RED, OFF);
      set_led_brightness(LED1RED, OFF);
      set_led_brightness(LED1BLUE, ON);
      printf("[LOG] Network successfully connected\n");
    } else {
      printf("[LOG] Network connection either failed or lost\n");
      //while ()
      /* Mode 3B: Unconnected, generating hotspot (LED) */
      hotspot_mode();
    }

    if (check_streaming()) {
      /* Mode 4A: Connected and streaming (LED1BLUE solid, LED2GREEN solid)*/
      set_led_brightness(LED2GREEN, ON);
    } else {
      set_led_brightness(LED2GREEN, OFF);
    }
    sleep(1);
  }

  return 0;
}
