#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/time.h>
#include <regex.h>
#include <stdbool.h>
#include "led-control.h"
#include "state-monitor.h"

#define LED1RED "red1"
#define LED1BLUE "blue1"
#define LED1GREEN "green1"
#define LED2RED "red2"
#define LED2BLUE "blue2"
#define LED2GREEN "green2"
#define ON 1
#define OFF 0


void sig_handler(int signo)
{
    if (signo == SIGINT)
        printf("\nreceived SIGINT\n");
    set_led_brightness(LED2RED, OFF);
    set_led_brightness(LED1BLUE, OFF);
    set_led_brightness(LED2GREEN, OFF);
    exit(1);
}

void power_off_all_leds() {
  set_led_brightness(LED1RED, OFF);
  set_led_brightness(LED2RED, OFF);
  set_led_brightness(LED1BLUE, OFF);
  set_led_brightness(LED2BLUE, OFF);
  set_led_brightness(LED1GREEN, OFF);
  set_led_brightness(LED2GREEN, OFF);
}

int main(int argc, char const *argv[]) {

  if (signal(SIGINT, sig_handler) == SIG_ERR) {
      printf("\ncan't catch SIG Handler\n");
      exit(1);
  }

  /* Check that all required LEDs have already been set up, else exit */
  if (!(led_exported(LED2RED) && led_exported(LED1BLUE) && led_exported(LED2GREEN))) {
    printf("[ERROR]: Some LEDs have not been configured correctly, program terminating.\n");
    exit(1);
  }

  power_off_all_leds();

  while (true) {
    /*Mode 2: Connecting to network(LED2RED blinking) - allow ~15 seconds to attempt this*/
    if (check_network_connected()) {
      /* Mode 4A: Connected and streaming (LED1BLUE solid, LED2GREEN solid)*/
      printf("[Log]: Connected to home network\n");
      set_led_brightness(LED1BLUE, ON);
    } else {
      printf("[Log]: Not connected to home network\n");
      set_led_brightness(LED1BLUE, OFF);
    }
    sleep(1);
  }
  return 0;
}
