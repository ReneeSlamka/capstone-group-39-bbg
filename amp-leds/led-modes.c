#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/time.h>
#include <regex.h>
#include <stdbool.h>
#include "led-control.h"
#include "state-monitor.h"

#define LED1RED "red1"
#define LED1BLUE "blue1"
#define LED1GREEN "green1"
#define LED2RED "red2"
#define LED2BLUE "blue2"
#define LED2GREEN "green2"
#define ON 1
#define OFF 0
#define CONNECTING_TIMEOUT_S 25

typedef enum {BOOT, CONNECTING, CONNECTED, STREAMING, HOTSPOT, NETWORK_CONFIGURATION} state;
state currentState = BOOT;
state nextState = BOOT;
int connectingCounter = 0;

void sig_handler(int signo)
{
    if (signo == SIGINT) {
      printf("\nreceived SIGINT\n");
      set_led_brightness(LED2RED, OFF);
      set_led_brightness(LED1BLUE, OFF);
      set_led_brightness(LED2BLUE, OFF);
      set_led_brightness(LED1GREEN, OFF);
      set_led_brightness(LED2GREEN, OFF);
      exit(1);
    }
}

void power_off_all_leds() {
  set_led_brightness(LED2RED, OFF);
  set_led_brightness(LED1BLUE, OFF);
  set_led_brightness(LED2BLUE, OFF);
  set_led_brightness(LED1GREEN, OFF);
  set_led_brightness(LED2GREEN, OFF);
}

void compute_next_state() {
  /* Mode 1: CONNECTING - RED1 blinking - on by default at boot - may remove */
  /* Transition 1: BOOT->CONNECTING */
  /* Transition 2: CONNECTED->CONNECTING */
  /* Transition 3: STREAMING->CONNECTING */
  if (((currentState == BOOT || currentState == CONNECTED || currentState == STREAMING)
    && !check_network_connected())
    || (currentState == HOTSPOT && !check_user_joined_hotspot() && check_wlan0_ssid_entered())) {
    nextState = CONNECTING;
  }
  /* Mode 2: CONNECTED - BLUE1 on */
  /* Transition 1: CONNECTING->CONNECTED */
  /* Transition 2: HOTSPOT->CONNECTED */
  /* Transition 3: STREAMING->CONNECTED */
  if ((currentState == CONNECTING && (connectingCounter < CONNECTING_TIMEOUT_S) && check_network_connected())
    || ((currentState == HOTSPOT || currentState == NETWORK_CONFIGURATION) && check_network_connected())
    || (currentState == STREAMING && check_network_connected()  && !check_streaming())) {
    nextState = CONNECTED;
  }

  /* Mode 3: STREAMING - BLUE1 on, GREEN2 on */
  /* Transition 1: CONNECTED->STREAMING */
  if (currentState == CONNECTED && check_streaming()) {
    nextState = STREAMING;
  }

  /* Mode 4: HOTSPOT - RED2 on, RED1 blink */
  /* Transition 1: CONNECTING->HOTSPOT */
  /* Transition 2: NETWORK_CONFIGURATION->HOTSPOT */
  if ((currentState == CONNECTING && (connectingCounter >= CONNECTING_TIMEOUT_S))
    || (currentState == NETWORK_CONFIGURATION && (!check_user_joined_hotspot())
    && !check_wlan0_ssid_entered())) { //Todo: may need to add ipCheck here
    nextState = HOTSPOT;
  }

  /* Mode 5: NETWORK_CONFIGURATION - RED2 on, RED1 off */
  if (currentState == HOTSPOT && check_user_joined_hotspot()) {
    nextState = NETWORK_CONFIGURATION;
  }
}

int main(int argc, char const *argv[]) {
  int red1BlinkValue = false;
  power_off_all_leds();

  if (signal(SIGINT, sig_handler) == SIG_ERR) {
      printf("\ncan't catch SIG Handler\n");
      exit(1);
  }

  /* Check that all required LEDs have already been set up, else exit */
  if (!(led_exported(LED2RED) && led_exported(LED1BLUE) && led_exported(LED2GREEN))) {
    printf("[ERROR]: Some LEDs have not been configured correctly, program terminating.\n");
    exit(1);
  }

  while (true) {
    compute_next_state();
    currentState = nextState;

    if (currentState == BOOT) {
      // should never go here
      connectingCounter = 0;
      set_led_brightness(LED1RED, ON);
    }
    if (currentState == CONNECTING) {
      // RED1 blink, others off
      connectingCounter++;
      set_led_brightness(LED2RED, OFF);
      set_led_brightness(LED1BLUE, OFF);
      set_led_brightness(LED2GREEN, OFF);

      set_led_brightness(LED1RED, red1BlinkValue);
      red1BlinkValue = !red1BlinkValue;
    }
    if (currentState == CONNECTED) {
      // BLUE1 on, others off
      connectingCounter = 0;
      set_led_brightness(LED2RED, OFF);
      set_led_brightness(LED1RED, OFF);
      set_led_brightness(LED2GREEN, OFF);

      set_led_brightness(LED1BLUE, ON);
    }
    if (currentState == STREAMING) {
      // BLUE1 on, GREEN2 on, others off
      connectingCounter = 0;
      set_led_brightness(LED2RED, OFF);
      set_led_brightness(LED1RED, OFF);

      set_led_brightness(LED2GREEN, ON);
      set_led_brightness(LED1BLUE, ON);
    }
    if (currentState == HOTSPOT) {
      // RED2 on, RED1 blink, others off
      connectingCounter = 0;
      set_led_brightness(LED2GREEN, OFF);
      set_led_brightness(LED1BLUE, OFF);

      set_led_brightness(LED2RED, ON);
      set_led_brightness(LED1RED, red1BlinkValue);
      red1BlinkValue = !red1BlinkValue;
    }
    if (currentState == NETWORK_CONFIGURATION) {
      // RED2 on, others off
      connectingCounter = 0;
      set_led_brightness(LED2GREEN, OFF);
      set_led_brightness(LED1BLUE, OFF);
      set_led_brightness(LED1RED, OFF);

      set_led_brightness(LED2RED, ON);
    }
    sleep(1);
  }

  return 0;
}
