#ifndef GPIO_LED_CONTROL_H_
#define GPIO_LED_CONTROL_H_

void export_gpio(int gpio);
void set_gpio_value(int gpio, int value);
void set_gpio_direction(int gpio, char direction[]);
void set_gpio_active_low(int gpio, int activeLowVal);
bool gpio_exported(int gpio);
void set_up_gpio(int gpio, char direction[], int activeLowVal, int value);

bool gpio_exported(int gpio) {
  char gpioStatusCmd[80];
  sprintf(gpioStatusCmd, "/sys/class/gpio/gpio%d/value", gpio);
  puts(gpioStatusCmd);
  int status = access(gpioStatusCmd, F_OK );
  if (status == -1) {
      printf("File for gpio%d doesn't exist. Execute \'echo $GPIO > export\' \
              in /sys/class/gpio as root where $GPIO = 49\n", gpio);
      return false;
  } else {
    printf("LOG: gpio%d was successfully configured", gpio);
    return true;
  }
}

void export_gpio(int gpio) {
  char exportGpioCmd[80];
  sprintf(exportGpioCmd, "echo %d > /sys/class/gpio/export", gpio);
  system(exportGpioCmd);
}

void set_up_gpio(int gpio, char direction[], int activeLowVal, int value) {
  if (!gpio_exported(gpio)) {
    export_gpio(gpio);
    set_gpio_direction(gpio, direction);
    set_gpio_active_low(gpio, activeLowVal);
    set_gpio_value(gpio, value);
    printf("[LOG]: GPIO%d configuration complete\n", gpio);
  }
}

void set_gpio_direction(int gpio, char direction[]) {
  char setGpioDirCmd[80];
  sprintf(setGpioDirCmd, "echo %s > /sys/class/gpio/gpio%d/direction", direction, gpio);
  system(setGpioDirCmd);
}

void set_gpio_active_low(int gpio, int activeLowVal) {
  char setGpioActiveLowCmd[80];
  sprintf(setGpioActiveLowCmd, "echo %d > /sys/class/gpio/gpio%d/active_low", activeLowVal, gpio);
  system(setGpioActiveLowCmd);
}


void set_gpio_value(int gpio, int value) {
  char gpioOnCmd[80];
  sprintf(gpioOnCmd, "echo %d > /sys/class/gpio/gpio%d/value", value, gpio);
  system(gpioOnCmd);
}

#endif
