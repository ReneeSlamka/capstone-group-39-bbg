#ifndef STATE_MONITOR_H_
#define STATE_MONITOR_H_

bool check_boot_complete();
bool check_ip_address_form(const char *str);
bool check_network_connected();
bool check_streaming();
bool check_user_joined_hotspot();
bool check_wlan0_ssid_entered();


bool check_boot_complete() {
  printf("[LOG] Checking if boot complete...\n");
  FILE *fp;
  char buff[50];
  /* Open the command for reading. */
  fp = popen("runlevel", "r");
  if (fp == NULL) {
    printf("[ERROR] Failed to run command\n");
    exit(1);
  }
  while (fgets(buff, sizeof(buff)-1, fp) != NULL) {
    printf("[LOG] runlevel read output: %s", buff);
  }
  pclose(fp);

  if (memchr(buff, '5', sizeof(buff))) {
    printf("[LOG] System boot is COMPLETE\n");
    return true;
  }

  printf("[LOG] System boot is INCOMPLETE\n");
  return false;
}

bool check_softAP_enabled() {
  return false;
}

bool check_ip_address_form(const char *str) {
  static const char *ipv4_pattern = "([0-9]{1,3}\\.|\\*\\.){3}([0-9]{1,3}|\\*){1}";
  regex_t re;
  int ret;

  if (regcomp(&re, ipv4_pattern, REG_EXTENDED) != 0)
      return false;

  ret = regexec(&re, str, (size_t) 0, NULL, 0);
  regfree(&re);

  if (ret == 0)
      return true;

  return false;
}


//Todo: check eth0 as well?
bool check_network_connected() {
  printf("[LOG] Checking for IP address...\n");
  FILE *fp;
  char buff[50];
  /* Open the command for reading. */
  fp = popen("ifconfig wlan0 | grep 'inet ' | awk -F'[: ]+' '{ print $4 }'", "r");

  if (fp == NULL) {
    printf("[ERROR] Failed to run command\n");
    exit(1);
  }
  /* Read the output a line at a time - output it. */
  fgets(buff, sizeof(buff)-1, fp);
  printf("%s", buff);
  pclose(fp);
  if (check_ip_address_form(buff)) {
    return true;
  }
  return false;
}

bool check_user_joined_hotspot() {
  printf("[LOG] Checking if user connected to Airhome hotspot...\n");
  FILE *fp;
  char buff[90];
  /* Open the command for reading. */
  fp = popen("arp-scan --quiet --interface=SoftAp0 --localnet --ignoredups", "r");
  if (fp == NULL) {
    printf("[ERROR] Failed to run command\n");
    exit(1);
  }
  /* Read the output a line at a time - target info should be on third line */
  /* If 3rd line is newline char, user not connected. Else will have ip address.*/
  for (int lineCounter = 0; (lineCounter < 3
    && fgets(buff, sizeof(buff)-1, fp) != NULL); lineCounter++) {
    printf("[LOG] Arp-scan output line(%d): %s\n", lineCounter, buff);
    if (lineCounter == 0) {
      char *result = strcasestr(buff, "ioctl: No such");
      if (result != NULL){
        printf("[LOG]: Hotspot not currently enabled\n");
        return false;
      }
    } else if (lineCounter == 2) {
      if (strlen(buff) == 0) {
        break;
      } else if (check_ip_address_form(buff)) {
        printf("[LOG] A user is currently connected to hotspot\n");
        return true;
      }
    }
  }
  printf("[LOG] No users currently connected to hotspot\n");
  return false;
}

bool check_wlan0_ssid_entered() {
  printf("[LOG] Checking if Airhome has an SSID for wlan0(wifi)...\n");
  FILE *fp;
  char buff[90];
  /* Open the command for reading. */
  fp = popen("iwgetid -r", "r");
  if (fp == NULL) {
    printf("[ERROR] Failed to run command\n");
    exit(1);
  }
  /* If no ssid above command will return nothing*/
  if (fgets(buff, sizeof(buff)-1, fp) != NULL) {
    printf("[LOG] Airhome has the following saved wifi SSID: %s\n", buff);
    return true;
  }
  printf("[LOG] Airhome does not have an SSID saved for wifi\n");
  return false;
}

bool check_streaming() {
  printf("[LOG] Checking if streaming audio...\n");
  FILE *fp;
  char buff[50];
  /* Open the command for reading. */
  fp = popen("cat /proc/asound/card0/pcm0p/sub0/hw_params", "r");
  if (fp == NULL) {
    printf("[ERROR] Failed to run command\n");
    exit(1);
  }
  /* Read the output a line at a time - output it. */
  if (fgets(buff, sizeof(buff)-1, fp) != NULL) {
    printf("[LOG] sub0/hw_paraps read output: %s", buff);
    /* If not streaming, output will just be "closed" on first line */
    char *result = strcasestr(buff, "closed");
    if (result == NULL) {
      printf("[LOG] Device is currently streaming\n");
      return true;
    }
  }
  printf("[LOG] Device is not currently streaming\n");
  return false;
}

#endif
