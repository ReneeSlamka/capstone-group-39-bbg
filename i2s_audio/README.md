# Contents #

## dts ##

The device tree source files.
These files depend on other device tree snippets in the kernel source so you will need to get the full kernel source to compile the DT.
More instructions inside.

## module ##

The driver kernel module.
This should be compiled on the BBGW.
More instructions inside

## wavgen.py ##

Script to generate raw PCM file with special values that can be seen on an oscilloscipe.