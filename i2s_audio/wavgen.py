#!/usr/bin/python
FS = 48000
CH = 2

f = open("pattern.pcm", 'wb')

def write_samples(pcm_file, sample_value, count):
    smp = bytearray([sample_value % 256, sample_value // 256])
    for i in range(0, CH * count):
        pcm_file.write(smp)

write_samples(f, 0, FS)
write_samples(f, 0xffff, FS)
write_samples(f, 0x5555, FS)
write_samples(f, 0xaaaa, FS)
write_samples(f, 0xff00, FS)
write_samples(f, 0x00ff, FS)
for i in range(0, 65536):
    write_samples(f, i, 10)
