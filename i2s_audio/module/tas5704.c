/*
 * ALSA SoC TAS5704 driver
 *
 * Based on PCM5102A driver by Josh Elliott
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <sound/soc.h>
#include <sound/soc-dai.h>
#include <sound/pcm.h>
#include <linux/of.h>

#define DRV_NAME "tas5704"

static struct snd_soc_codec_driver soc_codec_tas5704 = {

};

static struct snd_soc_dai_driver tas5704_dai = {
	.name = "tas5704-hifi",
	.playback = {
		.stream_name = "Playback",
		.channels_min = 2,
		.channels_max = 2,
		.rates = SNDRV_PCM_RATE_32000 |
		         SNDRV_PCM_RATE_44100 |
		         SNDRV_PCM_RATE_48000 |
		         SNDRV_PCM_RATE_88200 |
		         SNDRV_PCM_RATE_96000 |
		         SNDRV_PCM_RATE_176400 |
		         SNDRV_PCM_RATE_192000,
		.formats = SNDRV_PCM_FMTBIT_S16_LE | SNDRV_PCM_FMTBIT_S24_LE | SNDRV_PCM_FMTBIT_S32_LE,
	},
};

static int tas5704_probe(struct platform_device *pdev){
	printk("TAS5704 probe...\n");
	return snd_soc_register_codec(&pdev->dev, &soc_codec_tas5704, &tas5704_dai, 1);
}

static int tas5704_remove(struct platform_device *pdev){
	printk("TAS5704 remove...\n");
	snd_soc_unregister_codec(&pdev->dev);
	return 0;
}

#ifdef CONFIG_OF
static const struct of_device_id tas5704_dt_ids[] = {
	{ .compatible = "tas5704", },
	{}
};
MODULE_DEVICE_TABLE(of, tas5704_dt_ids);
#endif

static struct platform_driver tas5704_driver = {
	.probe = tas5704_probe,
	.remove = tas5704_remove,
	.driver = {
		.name = DRV_NAME,
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(tas5704_dt_ids),
	},
};

module_platform_driver(tas5704_driver);

MODULE_AUTHOR("Jason Jang <jcj83429@gmail.com>");
MODULE_DESCRIPTION("TAS5704 I2S codec dummy driver");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:" DRV_NAME);
