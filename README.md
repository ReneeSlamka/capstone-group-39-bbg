# Contents #

## i2s_audio ##

contains the kernel module and device tree for the i2s output


## amp-leds ##

Note:when making changes to the .service file on the BBGW, you must run "systemctl daemon-reload" afterwards. Other useful commands include:

* systemctl enable <name of.service file> - enable the target service
* systemctl status <name of.service file> - prints status data for that service (if it is enabled, active, etc)
* systemd-analyze plot > somefile.html - will output a time graph of when all services started (see the uploaded example)